﻿/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other people"s work. */

(function($){
	$.fn.wrapperURL = function(o) {
		var s = $.extend({}, o),
			check = /(\[URL=)|(\[url=)|( )|(\n)|(\))|(\()|(\[)|(\])|(\<)|(\>)/,
			re = /([^\"=]{2}|^)((https?|ftp):\/\/\S+[^\s.,> )\];'\"!?])/g,
			img_arr = ['png', 'jpg', 'gif', 'jpeg'];
			subst = ['$1<a href="$2" target="_blank">$2</a>', '$1<img style="margin:1px;" src="$2">'];
		
		
		this.each(function(){
			var result = '',
				arr = $(this).html().split(check),
				l = arr.length - 1;
			
			for (; l > -1; l--) {
				if((arr[l] != undefined) && (arr[l] != "")) {
					if(s.img_src){
						if(arr[l] == s.img_src) result = arr[l].replace(re, subst[1]) + result;
						else result = arr[l] + result;
					}
					else {
						if((arr[l] != " ") && (arr[l].length > 7) && ($.inArray(arr[l].split('.').pop(), img_arr) > -1)){
							function img_wrap(url, el){
								var img = new Image();
									img.src = url;
								
								img.onload = img.onerror = function(data) {
									if(data.type == "load") $(el).wrapperURL({'img_src' : img.src});
								};
							}
							
							img_wrap(arr[l], this);
						}
						result = arr[l].replace(re, subst[0]) + result;
					}
				}
			}
			$(this).html(result.replace(/^\s*/, '').replace(/\s*$/, '').replace(/((\r?\n)\s*\r?\n)+/g,'$2$2'));
		});
		
		return this;
	};
})(jQuery);




